# Adding a New View

So now that we can exit a file how can we view the files that have notes attached thus far?

If you recall from [Designing GNOME Tags](), we want to have a two views. One to write notes in and one to actually view all files with notes written. To do this we will be adding another view to our exiting one.

## `GtkStack` & Multiple Views

## Displaying documents with `GtkListBox`

## Creating a subclass of `GtkListBoxRow`