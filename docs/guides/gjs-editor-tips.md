---
title: Tips for Other Editing Environments
---
# Tips for Other Editing Environments

## Emacs

- If using Emacs, try js2-mode. It functions as a "lint" by highlighting missing semicolons and the like.

<!--## ESLint

- We have a plugin, `eslint-plugin-gjs`, which has excellent support for GJS development -->

