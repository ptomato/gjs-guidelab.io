---
title: GJS Guides
date: 2018-07-25 16:10:11
layout: IndexPage
---

# Developer Guides

    Welcome to GJS!

<ShowCaseBox title="GJS" subtitle="Tutorials for GJS">
<ShowCase link="gjs-style-guide.html" title="Style Guide" subtitle="The official style guide for GJS and GNOME projects written in GJS." image="" />
<ShowCase link="gjs-transition.html" title="Transition" subtitle="How does GJS compare to other JavaScript environments?" image="" />
<ShowCase link="gjs-legacy-class-syntax.html" title="Legacy Classes" subtitle="How do I use the deprecated Lang.Class objects?" image="" />
<ShowCase link="gjs-features-across-versions.html" title="Feature Compatibility" subtitle="Which features work in my version of GJS?" image="" />
</ShowCaseBox>

<ShowCaseBox title="GTK" subtitle="Gtk Tutorials">
<ShowCase link="gjs-gtk-application-packaging.html" title="GTK+ Application Packaging" subtitle="How do I package my GTK+ application with GJS?" image="" />
</ShowCaseBox>

<ShowCaseBox title="Gio" subtitle="Gio Tutorials">
<ShowCase link="gjs-basic-file-operations.html" title="Files in GJS" subtitle="Basic File Operations in GJS" image="" />
</ShowCaseBox>