---
title: GJS Showcase
date: 2018-07-25 16:10:11
---

Many amazing apps have been built in GJS. Check them out below!

<ShowCase link="https://wiki.gnome.org/Apps/Documents" title="GNOME Documents" subtitle="A document manager application designed to work with GNOME 3." image="" />

<ShowCase link="https://wiki.gnome.org/Apps/Weather" title="GNOME Weather" subtitle="An application that allows you to monitor the current weather conditions for your city, or anywhere in the world, and to access updated forecasts provided by various internet services." image="" />

<ShowCase link="https://wiki.gnome.org/Apps/Maps" title="GNOME Maps" subtitle="Maps gives you quick access to maps all across the world." image="" />

<ShowCase link="https://wiki.gnome.org/Apps/SoundRecorder" title="GNOME Sound Recorder" subtitle="A simple and modern sound recorder." image="" />
<!--


-->