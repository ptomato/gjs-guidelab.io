---
home: true
heroText: GJS
tagline: A Guide to JavaScript for GNOME
actionText: Get Started →
actionLink: /guides/
heroImage: /logo-alt.svg
features:
- title: Flexible
  details: Create Desktop Apps to fit your needs!
- title: Fast
  details: A fast JavaScript engine backed by all the resources of Mozilla's SpiderMonkey & Firefox development.
- title: Fully Featured
  details: Complete access to the GNOME stack, all in JavaScript
footer: MIT Licensed | Copyright © 2018 Evan Welsh | GJS, A GNOME Project 
---